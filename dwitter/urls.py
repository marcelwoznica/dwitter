from django.urls import path
from . import views
from .views import profile

app_name = "dwitter"

urlpatterns = [
    path("", views.Dashboard.as_view(), name="dashboard"),
    path("profile_list/", views.ProfileList.as_view(), name="profile_list"),
    path("profile/<int:pk>", views.ProfileFollowList.as_view(), name="profile"),
    # path("profile/<int:pk>", profile, name="profile"),
]
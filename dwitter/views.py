
from django.shortcuts import render
from django.views.generic import FormView, ListView, CreateView
from .forms import DweetForm
from dwitter.models import Profile

class Dashboard(FormView):
    template_name = "dwitter/dashboard.html"
    form_class = DweetForm
    success_url = '/'
    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        print("dupa")
        dweet = form.save(commit=False)
        dweet.user = self.request.user
        dweet.save()
        return super().form_valid(form)

class ProfileList(ListView):
    model = Profile
    template_name = "dwitter/profile_list.html"

    def get_context_data(self):
        context = super().get_context_data()
        context["profiles"] = Profile.objects.exclude(user=self.request.user)
        return context

class ProfileFollowList(ListView):
    model = Profile
    template_name = "dwitter/profile.html"

    def get_context_data(self):
        context = super().get_context_data()
        context["profile"] = Profile.objects.get(pk=self.kwargs['pk'])
        return context

    def post(self, request, pk):
        if not hasattr(request.user, 'profile'):
            missing_profile = Profile(user=request.user)
            missing_profile.save()
        profile = Profile.objects.get(pk=pk)
        if request.method == "POST":
            current_user_profile = request.user.profile
            data = request.POST
            action = data.get("follow")
            if action == "follow":
                current_user_profile.follows.add(profile)
            elif action == "unfollow":
                current_user_profile.follows.remove(profile)
            current_user_profile.save()
        return render(request, "dwitter/profile.html", {"profile": profile})

def profile(request, pk):
    profile = Profile.objects.get(pk=pk)
    if request.method == "POST":
        current_user_profile = request.user.profile
        data = request.POST
        action = data.get("follow")
        if action == "follow":
            current_user_profile.follows.add(profile)
        elif action == "unfollow":
            current_user_profile.follows.remove(profile)
        current_user_profile.save()
    return render(request, "dwitter/profile.html", {"profile": profile})